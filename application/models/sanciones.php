<?php
    class sanciones extends CI_Model {

        function get_sanciones() {
            $sql = "select idPlanilla, porcentaje, motivo from sanciones";
            $q = $this->db->query($sql);
            $data = array();
            for ($i=0; $i<$q->num_rows(); $i++) {
                $data[] = $q->row_array($i);
            }
            return $data;
        }

        function put_sanciones($data) {

            if(isset($data['idPlanilla'])){
                $sql = "select idPlanilla from sanciones where idPlanilla = ?";
                $q = $this->db->query($sql, $data['idPlanilla']);
                if ($q->num_rows() > 0) {
                    $this->db->where('idPlanilla', $data['idPlanilla']);
                    $this->db->update('sanciones', $data);
                }
            }
        }
    }
?>
