<?php
    class graficas extends CI_Model{


        function totales_prepa_qro(){
            // prepa qro
            $sql = "select count(1) as 'count'
                    from votantes v, programas p
                    where v.idprograma = p.id
                    and p.nombre like 'P% Qro'
                    and v.statusVotos = 'S'
                    union
                    select count(1)
                    from votantes v, programas p
                    where v.idprograma = p.id
                    and p.nombre like 'P% Qro'
                    and v.statusVotos = 'N'";
            $q = $this->db->query($sql);
            $data[] = $q->row_array(0);
            $data[] = $q->row_array(1);
            return $data;

        }

        function totales_prepa_cly(){
            // prepa celaya
            $sql = "select count(1) as 'count'
                    from votantes v, programas p
                    where v.idprograma = p.id
                    and p.nombre like '%cel'
                    and v.statusVotos = 'S'
                    union
                    select count(1)
                    from votantes v, programas p
                    where v.idprograma = p.id
                    and p.nombre like '%cel'
                    and v.statusVotos = 'N'";
            $q = $this->db->query($sql);
            $data[] = $q->row_array(0);
            $data[] = $q->row_array(1);
            return $data;

        }

        function totales_profesional(){
            // profesional qro
            $sql = "select count(1) as 'count'
                    from votantes v, programas p
                    where v.idprograma = p.id
                    and p.nombre not like 'P%'
                    and v.statusVotos = 'S'
                    union
                    select count(1)
                    from votantes v, programas p
                    where v.idprograma = p.id
                    and p.nombre not like 'P%'
                    and v.statusVotos = 'N'";
            $q = $this->db->query($sql);
            $data[] = $q->row_array(0);
            $data[] = $q->row_array(1);
            return $data;

        }

        function get_votantes_totales( ){
            $sql = 'select p.nombre, count(p.nombre) as conteo
                    from programas p, votantes v
                    where p.id = v.idprograma
                    group by p.nombre';
            $q = $this->db->query($sql);

            $data = array();
            for( $i = 0; $i < $q->num_rows(); $i++ )
                $data[] = $q->row_array($i);
            return $data;
        }

        function get_votos_totales(  ) {
            $sql = "select p.nombre, count(p.nombre) as conteo
                    from programas p, votantes v
                    where p.id = v.idprograma
                            and v.statusvotos = 'S'
                    group by p.nombre";
            $q = $this->db->query( $sql );
            $data = array();
            for( $i = 0; $i < $q->num_rows(); $i++ )
                $data[] = $q->row_array($i);
            return $data;
        }


        function planillas_votos( ){
            $sql = "select e.nombre as eleccion, p.nombre as planilla, v.votos as votos , s.porcentaje as sancion
                    from votos v, planillas p, elecciones e , sanciones s
                    where v.idplanilla = p.id and p.ideleccion = e.id and s.idplanilla = p.id
                    and e.fechafin < now()
                    order by e.nombre, v.votos desc";
            $q = $this->db->query( $sql );
            $data = array();
            for( $i = 0; $i < $q->num_rows(); $i++ )
                $data[] = $q->row_array($i);
            return $data;
        }

        function votantes_sede(){
                /* CELAYA */
            // total celaya
            $sql = "select count(1) as conteo
                    from votantes v, programas p
                    where v.idprograma = p.id
                        and p.nombre like '%cel'";
            $q = $this->db->query($sql);
            $data['total_pcly'] = $q->row()->conteo;

            $sql = "select count(1) as conteo
                    from votantes v, programas p
                    where v.idprograma = p.id
                        and p.nombre like '%cel'
                    and v.statusvotos = 'S'";
            $q = $this->db->query($sql);
            $data['votos_pcly'] = $q->row()->conteo;

                /* PREPA QRO */
            // total prepa qro
            $sql = "select count(1) as conteo
                    from votantes v, programas p
                    where v.idprograma = p.id
                        and p.nombre like '%qro'";
            $q = $this->db->query($sql);
            $data['total_pqro'] = $q->row()->conteo;

            $sql = "select count(1) as conteo
                    from votantes v, programas p
                    where v.idprograma = p.id
                        and p.nombre like '%qro'
                    and v.statusvotos = 'S'";
            $q = $this->db->query($sql);
            $data['votos_pqro'] = $q->row()->conteo;

                /* PROFESIONAL */
            // total profesional
            $sql = "select count(1) as conteo
                    from votantes v, programas p
                    where v.idprograma = p.id
                        and p.nombre not like 'p%'";
            $q = $this->db->query($sql);
            $data['total_prof'] = $q->row()->conteo;

            $sql = "select count(1) as conteo
                    from votantes v, programas p
                    where v.idprograma = p.id
                        and p.nombre like 'P%'
                    and v.statusvotos = 'S'";
            $q = $this->db->query($sql);
            $data['votos_prof'] = $q->row()->conteo;

            return $data;
        }
    }
?>
