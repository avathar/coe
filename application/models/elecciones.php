<?php
    class Elecciones extends CI_Model
    {
        // Tercer nivel.
        // Tablas: Sociedad_Prog y Votaciones.
        // $id_prog fuente programas.php o votante.php
        // Devueve id y nombre de las sociedades pertenecientes -
        // Al programa que se recibe como parámetro.
        function get_elecciones($id_prog) {
            $sql = "select * from elecciones where id in (select idEleccion from sociedad_programa where idPrograma = ?)";
            $q = $this->db->query($sql, $id_prog);
            $data = array();
            for( $i=0; $i<$q->num_rows(); $i++ ) {
                $data[] = $q->row_array($i);
            }
            return $data;
        }

        function get_fiff($idEleccion) {
            $sql = "select fechaInicio, fechaFin from elecciones where id = ?";
            $q = $this->db->query($sql, $idEleccion);
            $data = $q->row_array();
            return $data;
        }
    }
