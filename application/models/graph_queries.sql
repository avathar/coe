/**
    students by program
*/

select p.nombre, count(p.nombre) from programas p, votantes v where p.id = v.idprograma group by p.nombre;

/**
    students who voted
*/

select p.nombre, count(p.nombre) from programas p, votantes v where p.id = v.idprograma and v.statusvotos = 'S'group by p.nombre;


