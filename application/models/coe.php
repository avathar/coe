<?php

class coe extends CI_Model{
    function valida_contingencia( $mcoe, $pass ){
        $sql = "select password from COE where matricula = ?";
        $q = $this->db->query($sql, $mcoe);
        if ($q->num_rows() > 0) {
            $db_pass = $q->row()->password;
            return $pass == $db_pass;
        }
        return 0;
    }


    // Do magic here
    function valida_login($mcoe, $pass){
        $sql = "select password, nivel from COE where matricula = ?";
        $q = $this->db->query($sql, $mcoe);
        if ($q->num_rows() > 0) {
            $db_pass = $q->row()->password;
            $db_nivel = $q->row()->nivel;
            if($db_pass == $pass){
                if($db_nivel == "S")
                    return array(true,$db_nivel);
                else
                    return array(false, null);    
            }else{
                return array(false, null);
            }
        }
        return array(false, null);
    }

    function valida_usuario( $mcoe ){
        $sql = "select matricula from COE where matricula = ?";
        $q = $this->db->query($sql, $mcoe);
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }
}
