<?php
    class votantes extends CI_Model{
        // Primer nivel.
        // Tabla: Votantes.
        // $matricula fuente vista.
        // Devuelve datos de alumno.
        function get_alumno( $matricula ){
            // var_dump($matricula);
            $sql = 'select * from votantes where matricula = ?';
            $q = $this->db->query( $sql, $matricula );
            $row = $q->row_array();
            return $row;
        }



        function vota_alumno($matricula, $all_planillas){
            $sql = "select statusVotos from votantes where matricula = ?";
            $q = $this->db->query($sql, $matricula);
            $row = $q->row();

            // check again if not voted
            if($row->statusVotos == 'N'){
                // change student's status to 'yes'
                foreach ($all_planillas as $planilla) {
                    $add_vote = "update votos set votos = votos +1 where idPlanilla = ?";
                    $vote_query = $this->db->query($add_vote, $planilla);
                }
                $change_status = "update votantes set statusvotos = 'S' where matricula = ? ";
                $status_query = $this->db->query($change_status, $matricula);
                // save to log for statistics
                $this->util->registra_bitacora($matricula, "Registro de voto");
            }else{
                // if dude gets here, somehow got to votaciones/vota with statusVoto = 'S'
                // but we too smart and check again before update and log that :D
              $this->util->registra_bitacora($matricula, "Intento voto doble");
            }
        }

        function ya_voto($matricula){
            $sql = "select statusVotos from votantes where matricula = ?";
            $q = $this->db->query($sql, $matricula);
            $row = $q->row();
            return $row->statusVotos == 'S';
        }

        function get_votantes_totales(){
            $sql = 'select p.nombre, count(p.nombre) as conteo
                    from programas p, votantes v
                    where p.id = v.idprograma
                    group by p.nombre';
            $q = $this->db->query($sql);
            $data = array();
            for( $i=0; $i < $q->num_rows(); $i++ )
                $data[] = $q->row_array($i);

            return $data;
        }

    }
