<?php

	class mesa extends CI_Model
	{
		// Quinto nivel B.
		// Tablas: Mesa y Cargo
		// $id_plan fuente planillas.php
		// Devuelve un arr con la mesa de la planilla dada en -
		// el formato de nombre, cargo.

		function get_mesa( $id_plan ) {

			$sql = "select c.nombre as cargo, m.nombre as nombre from cargo c inner join mesa m on c.id = m.idCargo where m.idPlanilla = ?";
			$q = $this->db->query( $sql, $id_plan );
			$data = array(); 
			for( $i=0; $i<$q->num_rows(); $i++ ) {
				$data[] = $q->row_array($i);
			}

			return $data;
		}
	}
?>
