<?php
	class planillas extends CI_Model{
		// Cuarto nivel.
		// Tabla: Planillas.
		// $id_soc fuente sociedades.php
		// Devuelve todas las planillas pertenecientes a la -
		// Sociedad recibida como parámetro.
		function get_planillas( $id_soc ){
			$sql = "select id, nombre, slogan, logo, foto, color from planillas where idEleccion = ? order by slogan desc";
			$q = $this->db->query( $sql, $id_soc );
			$data = array();
			for( $i=0; $i<$q->num_rows(); $i++ ){
				$data[] = $q->row_array($i);
			}
			return $data;
		}

		function getall_planillas( ){
			$sql = "select id, nombre from elecciones";
			$q = $this->db->query( $sql );
			$data = array();
			for( $i=0; $i<$q->num_rows(); $i++ ){
				$data[] = $q->row_array($i);
			}
			for($i = 0; $i<count($data); $i++){
				$sql = "select id, nombre, color from planillas where idEleccion = ? and nombre not like 'NULO'";
				$q = $this->db->query( $sql, $data[$i]['id'] );
				for( $j=0; $j<$q->num_rows(); $j++ ){
					$data[$i]['planillas'][] = $q->row_array($j);
					$sql = "select idPlanilla, porcentaje, motivo from sanciones where idPlanilla = ?";
		            $q2 = $this->db->query($sql, $data[$i]['planillas'][$j]['id'] );
		            for ($k=0; $k<$q2->num_rows(); $k++) {
		                $data[$i]['planillas'][$j]['sancion'] = $q2->row_array($k);
		            }
				}
			}
			return $data;
		}
	}
?>
