<?php
    class votos extends CI_Model
    {
        // Quinto nivel A.
        // Tabla: Votos.
        // $id_plan fuente planillas.php
        // Devuelve la cantidad de votos que ha recibido -
        // una planilla dada.
        function get_votos_totales(  ) {
            $sql = "select p.nombre, count(p.nombre) as conteo
                    from programas p, votantes v
                    where p.id = v.idprograma
                    and v.statusvotos = 'S'
                    group by p.nombre";
            $q = $this->db->query( $sql );
            $data = array();
            for( $i=0; $i<$q->num_rows(); $i++ ) {
                $data[] = $q->row_array($i);
            }
            return $data;
        }
    }
?>
