<?php
	class Programas extends CI_Model
	{
		// Segundo nivel.
		// Tabla: Programas.
		// $id_prog fuente votantes.php
		// Devuelve nombre del programa del alumno.
		function get_programa( $id_prog )
		{
			$sql = 'select * from programas where id = ?';
			$q = $this->db->query( $sql, $id_prog );
			$row = $q->row_array();
			return $row;
		}
	}
?>
