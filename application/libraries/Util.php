<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Util {

    public function registra_bitacora($user, $info){
        $CI = &get_instance();
        $CI->load->model('bitacora');
        $d['id'] = 0;
        $d['matricula'] = $user;
        $d['evento'] = $info;
        $CI->bitacora->put_reg($d);
    }
}
