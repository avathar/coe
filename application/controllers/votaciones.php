<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Votaciones extends CI_Controller {

    public function index(){
        $this->load->view('header');
        $this->load->view('votaciones/login');
        $this->load->view('footer');
    }

    public function tiempo() {
        $this->load->view('votaciones/tiempo');
    }

    public function login(){
        if($this->input->post( )){
            $this->load->model('votantes');
            $usr = $this->input->post('matricula');
            $pwd = $this->input->post('password');
            $data['alumno'] = $this->votantes->get_alumno($usr);

            // check if user in votantes list
            if($data['alumno']){
                if($this->votantes->ya_voto($usr)){
                    echo 'voto';
                    return;
                }
            }
            // is it an emergency?
            if( $this->input->post('usuario') ){
                $this->load->model('coe');
                $coe = $this->input->post('usuario');
                $valido = $this->coe->valida_contingencia($coe,$pwd);
                if(!$valido){
                    echo 'false';
                    return;
                }
                $this->load->library('session');
                $user_data = array('matricula' => $usr );
                $this->session->set_userdata($user_data);
                $this->util->registra_bitacora($coe, "Usa contingencia para ".$usr);
                echo 'true';
                return;
            // then is regular login
            }else{

                // To connect to an SSL IMAP or POP3 server with a self-signed certificate,
                // add /ssl/novalidate-cert after the protocol specification:
                $mbox = imap_open("{outlook.office365.com:993/imap/ssl/novalidate-cert}", $usr . "@itesm.mx", $pwd, NULL, 1, array('DISABLE_AUTHENTICATOR' => 'GSSAPI'));
                if ($mbox){
                    $this->load->library('session');
                    $user_data = array('matricula' => $usr );
                    $this->session->set_userdata($user_data);
                    $this->util->registra_bitacora($usr, "Inicia Sesion");
                    echo "true";
                    return;
                }

                /*
                //  LDAP Validation Service NOT WORKING
                $server='ldap://svcs.itesm.mx';
                $ds=ldap_connect($server);
                if ($ds) {
                    $r=ldap_bind($ds, $usr."@itesm.mx", $pwd);
                    if(!$r) die("false");
                    $this->load->library('session');
                    $user_data = array('matricula' => $usr);
                    $this->session->set_userdata($user_data);
                    $this->util->registra_bitacora($usr, "Inicia Sesion");
                    ldap_close($ds);
                    echo "true";
                    return;
                }
                */

            }
        }else{
            echo 'false';
            return;
        }
    }


    public function elecciones( ){
        $this->load->library('session');

        if(  $this->session->userdata('matricula') ){
            $alumno = $this->session->userdata('matricula');
            // models loading
            $this->load->model('votantes');
            $this->load->model('programas');
            $this->load->model('elecciones');
            $this->load->model('planillas');
            $this->load->model('mesa');
            $data['alumno'] = $this->votantes->get_alumno($alumno);
            $data['programa'] = $this->programas->get_programa($data['alumno']['idPrograma']);

            // saving data for UI elements
            $user_data = array(
                'matricula' => $data['alumno']['matricula'],
                'programa'  => $data['programa']['nombre']
            );
            $this->session->set_userdata($user_data);

            // continue getting election data
            $data['eleccion'] = $this->elecciones->get_elecciones($data['programa']['id']);
            $i = 0;
            $clean_elecciones = array();
            foreach ($data['eleccion'] as $eleccion) {
                $hoy = time();
                $inicio = strtotime($eleccion['fechaInicio']);
                $fin = strtotime($eleccion['fechaFin']);
                if($hoy < $inicio || $hoy > $fin){
                    // BAD DATE DO SOMETHING
                }else{
                    $clean_elecciones[] = $data['eleccion'][$i];
                }
                $i++;
            }

            // check if we still have available elections
            if(count($clean_elecciones) == 0){
                redirect('votaciones/tiempo');
            }
            // we do, so dump clean to data
            $data['eleccion'] = $clean_elecciones;
            $i = 0;
            foreach($data['eleccion'] as $eleccion){
                $data['eleccion'][$i++]['planilla'] = $this->planillas->get_planillas($eleccion['id']);
            }

            foreach ($data['eleccion'] as $eleccion) {
                foreach($eleccion['planilla'] as $planilla){
                    $data['mesa'][$planilla['id']] = $this->mesa->get_mesa($planilla['id']);
                }
            }
            // at view we'll use $mesa['planilla_id'] to know which one it is
            // so foreaching every planilla, we'll be able to know it's id and foreach $mesa to get it's data
            // kind of ugly 'cause weird key values, but it should work
            $this->load->view('header',$data);
            $this->load->view('votaciones/elecciones',$data);
            $this->load->view('footer');
        }else{// redirect to login form
                redirect('votaciones/');
        }
    }

    public function _maneja_error( $errorcode ){
        $data['errorcode'] = $errorcode;
        $this->load->view('header');
        $this->load->view('votaciones/login',$data);
        $this->load->view('footer');
    }

    public function vota( ){
        $this->load->library('session');
        $matricula = $this->session->userdata('matricula');
        if($this->input->post( ) && $matricula ){
            $this->load->model('votantes');
            $this->load->model('bitacora');
            $i = 1;
            $all_planillas = array();
            while( ($planilla = $this->input->post('planilla'.$i)) ){
                $all_planillas[] = (int)$planilla;
                $i++;
            }
            $this->votantes->vota_alumno($matricula, $all_planillas);
            // delete session data
            $this->session->unset_userdata('matricula');
            $data['voted'] = true;
            // show thanks and graphs
            $this->load->view('header');
            $this->load->view('votaciones/graficas',$data);
            $this->load->view('footer');
        }else{
            $this->load->view('header');
            $this->load->view('votaciones/login');
            $this->load->view('footer');
        }
    }

    function graficas(){
        $this->load->view('header');
            $this->load->view('votaciones/graficas');
            $this->load->view('footer');
    }

    function tablas( ){
        $this->load->view('header');
        $this->load->view('votaciones/tablas');
        $this->load->view('footer');
    }
}

/* End of file votaciones.php */
/* Location: ./application/controllers/votaciones.php */


