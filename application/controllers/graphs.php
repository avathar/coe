<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Graphs extends CI_Controller {

    function participacion( ){
        header('Content-type: application/json');
        $this->load->model('graficas');

        $votantes = $this->graficas->get_votantes_totales();
        $votos = $this->graficas->get_votos_totales();

        for ($i=0; $i < count($votantes) ; $i++) {
            $total = (float)$votantes[$i]['conteo'];
            $votantes[$i]['conteo'] = $total;
            $votantes[$i]['t_votantes'] = $total;
            $votantes[$i]['total'] = $total;
            $votantes[$i]['t_votos'] = 0;
            $votantes[$i]['votos'] = 0;
            for ($j=0; $j <count($votos) ; $j++) {
                if($votantes[$i]['nombre'] == $votos[$j]['nombre']){
                    $votantes[$i]['t_votos'] = $votos[$j]['conteo'];
                    $votantes[$i]['t_votantes'] -= $votantes[$i]['t_votos'];
                    $votantes[$i]['votos'] = $votos[$j]['conteo'] / $total;
                    $votantes[$i]['conteo'] = ($votantes[$i]['conteo'] - $votos[$j]['conteo']) / $total;
                    break;
                }
            }
            if($votantes[$i]['conteo'] > 1) $votantes[$i]['conteo'] = 1;
        }

        $jsonData['cols'] = array(
            array("label"=>"Programa", "type"=>'string'),
            array("label"=>"Votaron", "type"=>'number'),
            array("label"=>"No votaron", "type"=>'number')
        );

        foreach ($votantes as $data ) {
            $votos = number_format($data['votos']*100,2);
            $censo = number_format($data['conteo']*100,2);
            $jsonData['rows'][]['c'] =  array(
                array("v"=> $data['nombre']),
                array("v"=> (float) $votos, "f"=>$votos."% : ".$data['t_votos']." de ".$data['total']),
                array("v"=> (float) $censo, "f"=>$censo."% : ".$data['t_votantes']." de ".$data['total'])
            );
        }
        echo json_encode($jsonData);
    }

    function tabla_participacion(){
        header('Content-type: application/json');
        $this->load->model('graficas');

        $votantes = $this->graficas->get_votantes_totales();
        $votos = $this->graficas->get_votos_totales();

        for ($i=0; $i < count($votantes) ; $i++) {
            $total = (float)$votantes[$i]['conteo'];
            $votantes[$i]['conteo'] = $total;
            $votantes[$i]['t_votantes'] = $total;
            $votantes[$i]['total'] = $total;
            $votantes[$i]['t_votos'] = 0;
            $votantes[$i]['votos'] = 0;
            for ($j=0; $j <count($votos) ; $j++) {
                if($votantes[$i]['nombre'] == $votos[$j]['nombre']){
                    $votantes[$i]['t_votos'] = $votos[$j]['conteo'];
                    $votantes[$i]['t_votantes'] -= $votantes[$i]['t_votos'];
                    $votantes[$i]['votos'] = $votos[$j]['conteo'] / $total;
                    $votantes[$i]['conteo'] = ($votantes[$i]['conteo'] - $votos[$j]['conteo']) / $total;
                    break;
                }
            }
            if($votantes[$i]['conteo'] > 1) $votantes[$i]['conteo'] = 1;
        }

        $jsonData['cols'] = array(
            array("label"=>"Programa", "type"=>'string'),
            array("label"=>"Votaron", "type"=>'number'),
            array("label"=>"En censo", "type"=>'number'),
            array("label"=>" % Participacion", "type"=>'number')
        );

        foreach ($votantes as $data ) {
            $votos = number_format($data['votos']*100,2);
            $censo = number_format($data['conteo']*100,2);
            $jsonData['rows'][]['c'] =  array(
                array("v" => $data['nombre']),
                array("v" => $data['t_votos']),
                array("v" => $data['total']),
                array("v" => $votos." %")
            );
        }
        echo json_encode($jsonData);
    }

    function totales_profesional(){
        header('Content-type: application/json');
        $this->load->model('graficas');
        $votantes = $this->graficas->totales_profesional();
        $jsonData['cols'] = array(
            array("label"=>"Participación", "type"=>'string'),
            array("label"=>"Cantidad", "type"=>'number')
        );
        $jsonData['rows'][]['c'] =  array(
            array("v"=> "Votaron"),
            array("v"=> (int)$votantes[0]['count'])
        );
        $jsonData['rows'][]['c'] =  array(
            array("v"=> "No Votaron"),
            array("v"=> (int)$votantes[1]['count'])
        );
        if($votantes[0]['count'] == "0" &&  $votantes[1]['count'] == "0"){
            $jsonData['rows'][]['c'] =  array(
                array("v"=> "Sin participación"),
                array("v"=> 1)
            );
        }
        echo json_encode($jsonData);
    }

    function totales_prepa_qro(){
        header('Content-type: application/json');
        $this->load->model('graficas');
        $votantes = $this->graficas->totales_prepa_qro();
        $jsonData['cols'] = array(
            array("label"=>"Participación", "type"=>'string'),
            array("label"=>"Cantidad", "type"=>'number')
        );
        $jsonData['rows'][]['c'] =  array(
            array("v"=> "Votaron"),
            array("v"=> (int)$votantes[0]['count'])
        );
        $jsonData['rows'][]['c'] =  array(
            array("v"=> "No Votaron"),
            array("v"=> (int)$votantes[1]['count'])
        );
        if($votantes[0]['count'] == "0" &&  $votantes[1]['count'] == "0"){
            $jsonData['rows'][]['c'] =  array(
                array("v"=> "Sin participación"),
                array("v"=> 1)
            );
        }
        echo json_encode($jsonData);
    }

    function totales_prepa_cly(){
        header('Content-type: application/json');
        $this->load->model('graficas');
        $votantes = $this->graficas->totales_prepa_cly();
        $jsonData['cols'] = array(
            array("label"=>"Participación", "type"=>'string'),
            array("label"=>"Cantidad", "type"=>'number')
        );
        $jsonData['rows'][]['c'] =  array(
            array("v"=> "Votaron"),
            array("v"=> (int)$votantes[0]['count'])
        );
        $jsonData['rows'][]['c'] =  array(
            array("v"=> "No Votaron"),
            array("v"=> (int)$votantes[1]['count'])
        );
        if($votantes[0]['count'] == "0" &&  $votantes[1]['count'] == "0"){
            $jsonData['rows'][]['c'] =  array(
                array("v"=> "Sin participación"),
                array("v"=> 1)
            );
        }
        echo json_encode($jsonData);
    }


    function resultados(){
        header('Content-type: application/json');
        $this->load->model('graficas');
        $planillas = $this->graficas->planillas_votos();

        $jsonData['cols'] = array(
            array("label"=>"Eleccion", "type"=>'string'),
            array("label"=>"Planilla", "type"=>'string'),
            array("label"=>"Votos", "type"=>'number'),
            array("label"=>"Sancion (%)", "type"=>'number'),
            array("label"=>"Votos Netos", "type"=>'number')
        );

        foreach ($planillas as $data ) {
            $netos = number_format($data['votos'] - ($data['votos'] * ($data['sancion']/100)),0);
            $jsonData['rows'][]['c'] =  array(
                array("v"=> $data['eleccion']),
                array("v"=> $data['planilla']),
                array("v"=> (int) $data['votos']),
                array("v"=> number_format($data['sancion'],2)." %"),
                array("v"=> $netos)
            );
        }
        echo json_encode($jsonData);
    }

}
