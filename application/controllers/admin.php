<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
    Cómo necesito los datos para insertar en la bitácora:
    $datos['id'] = 0;
    $datos['matricula'] = "A0120...";
    $datos['evento'] = "Votó";
*/

class Admin extends CI_Controller {


    function logout( ){
        if($this->_valida_sesion( )){
            $this->session->sess_destroy();
        }
        redirect('/');
    }

    function graficas(){
        if($this->_valida_sesion( )){
            $this->load->view('admin/header');
            $this->load->view('admin/graficas');
            $this->load->view('admin/footer');
        }else{
            redirect('/admin/');
        }
    }

    function menu(){
        if($this->_valida_sesion( )){
            $data['login'] = 0;
            $this->load->view('admin/header',$data);
            $this->load->view('admin/menu');
            $this->load->view('admin/footer');
        }else{
            redirect('/admin/');
        }
    }

    function index( ){
        if($this->_valida_sesion( )){
            redirect('admin/menu');
        }else{
            $data['login'] = 0;
            $this->load->view('admin/header',$data);
            $this->load->view('admin/login');
            $this->load->view('admin/footer');
        }
    }

    function _valida_sesion( ){
        $this->load->library('session');
        $this->load->model('coe');
        if( $this->session->userdata('usuario') ){
            $user = $this->session->userdata('usuario');
            return $this->coe->valida_usuario($user);
        }
        return 0;
    }

    function login( ){
        if($this->input->post( )) {
            $user = $this->input->post('usuario');
            $pass = $this->input->post('clave');
            $this->load->model('coe');
            $answer = $this->coe->valida_login($user,$pass);
            if($answer[0]){
                $this->load->library('session');
                $user_data = array(
                    'usuario' => $user,
                    'nivel'  => $answer[1]
                );
                $this->session->set_userdata($user_data);
                $this->util->registra_bitacora($user, "Inicia session");
                echo "true";
                return;
            // login failed
            }
            echo "false";
            return;
        }
       echo "empty";
       return;
    }

    // so far, this will only pasar por la tienda
    function sanciones(){
        if ($this->_valida_sesion()) {
            $this->load->model('planillas');
            $this->load->model('sanciones');

            $data['elecciones'] = $this->planillas->getall_planillas();
            $data['sanciones'] = $this->sanciones->get_sanciones();

            $this->load->view('admin/header');
            $this->load->view('admin/sanciones',$data);
            $this->load->view('admin/footer');
        } else{
            redirect('/admin/admin_login');
        }
    }

    function aplica_sanciones() {
        $this->load->library('session');
        if ($this->_valida_sesion()) {
            if ($this->input->post()) {
                $this->load->model('sanciones');
                $this->load->model('bitacora');

                $pots = $this->input->post();
                $i = 0;
                foreach ($pots as $k => $v) {
                    if (is_numeric($k)) {
                        $nums[$i]['idPlanilla'] = $k;
                        if($v == ""){
                            $nums[$i]['porcentaje'] = 0;
                        }else{
                            $nums[$i]['porcentaje'] = $v;
                        }
                        $i--;
                    } else {
                        $nums[$i]['motivo'] = $v;
                    }
                    $i++;
                }
                foreach ($nums as $ns) {
                    $this->sanciones->put_sanciones($ns);
                }
                $usr = $this->session->userdata('usuario');
                $this->util->registra_bitacora($usr,"Guarda sanciones");
            }
            redirect('/admin/graficas');
        }else{
            redirect('/admin/login');
        }
    }
}
?>
