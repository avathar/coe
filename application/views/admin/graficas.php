<script type="text/javascript" src="http://www.google.com/jsapi">  </script>
<script type="text/javascript">

google.load('visualization', '1');   // Don't need to specify chart libraries!
google.setOnLoadCallback(drawVisualization);

function drawVisualization( ){
    var jsonData = jQuery.ajax({
        url: "<?php echo base_url() ?>index.php/graphs/resultados",
        dataType:"json",
        async: false
    }).responseText;

    var wrapper = new google.visualization.ChartWrapper({
        chartType: 'Table',
        dataTable: jsonData,
        options: {
            title: 'Resultados',
            width: 500
        }
    });
    wrapper.draw('rgraph');

}
</script>

<br>
<div id="rgraph" align="center"></div>

<p align="center">
<?php echo anchor('admin/menu','Regresar al Menu' ); ?>
</p>
