<script>
$(document).ready(function(){
    $('form').submit(function(event) {
        event.preventDefault();
        var usuario = $('input[name=usuario]').val();
        var clave = $(this).children('input[name=clave]').val();
        var form = $(this);
        form.prev().hide();
        var data = {'usuario': usuario, 'clave': clave };
        $.ajax({
            type: "POST",
            url: '<?php echo base_url() ?>index.php/admin/login',
            data: data,
            success: function(data){
                if(data == "true"){
                    window.location = "<?php echo base_url() ?>index.php/admin/menu";
                }else{
                    form.prev().html(' Matricula y/o contraseña inválida').show('easeInOutExpo');
                }
            }
        });
    });
});
</script>

<div id="login">
    <div class="formcont">
        <h1> Admin Login </h1>
        <div id='errordialog'> </div>
        <?php
            echo form_open('',array('id'=>'form'));
                echo form_input(array('name'=>'usuario','placeholder'=>'Usuario COE'));
                echo '<div class="clear"> </div>';
                echo form_password(array('name'=>'clave','placeholder'=>'Contraseña COE'));
                echo '<div class="clear"> </div>';
                echo form_submit('login','Entrar');
            echo form_close( );
        ?>
    </div>

</div>
