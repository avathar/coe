

<div class="sanciones">
    <div class="formcont">
        <h1>Sanciones</h1>
        <?php echo form_open('admin/aplica_sanciones',array('id'=>'form')); ?>
            <?php foreach ($planillas as $plan): ?>
                <div class="d-label">
                    <?php echo $plan['nombre'] ?>
                </div>
                <div class="motivos">
                    <?php foreach ($sanciones as $san): ?>
                        <?php if ($plan['id'] == $san['idPlanilla']) { ?>
                            <?php echo form_input(array('name'=>$plan['id'],'placeholder'=>'00', 'id' => 'porc', 'value' => $san['porcentaje'])); ?> %
                            <?php echo '<textarea name="motivo'.$plan['id'].'" id="text" placeholder="Motivos" >'.$san['motivo'].'</textarea>'; ?>
                        <?php } ?>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
            <?php echo form_submit('send','Enviar'); ?>
        <?php echo form_close(); ?>
    </div>
</div>
