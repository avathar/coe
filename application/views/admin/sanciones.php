
<script>
$(document).ready(function() {
    $('#tabify').tabify();
    $("form div:first").css("display", "block");
});
</script>
<div class="center">
    <h1>Sanciones</h1>
</div>

<ul id="tabify">
    <?php foreach ($elecciones as $each): ?>
        <li> <a href="#el<?php echo $each['id'] ?>"> <?php echo $each['nombre'] ?></a> </li>
    <?php endforeach; ?>
</ul>
<div class="sanciones">
    <div class="formcont">
        <?php echo form_open('admin/aplica_sanciones',array('id'=>'form')); ?>
            <?php foreach ($elecciones as $each): ?>
                <div id="el<?php echo $each['id'] ?>">
                <?php foreach ($each['planillas'] as $plan): ?>
                    <div class="sancion">
                        <h3> <?php echo $plan['nombre'] ?> </h3>
                        <?php echo form_input(array('name'=>$plan['id'],'placeholder'=>'00', 'id' => 'porc', 'value' => $plan['sancion']['porcentaje'])); ?> %
                        <?php echo '<textarea name="motivo'.$plan['id'].'" id="text" placeholder="Motivos" >'.$plan['sancion']['motivo'].'</textarea>'; ?>
                    </div>
                <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
<?php echo form_submit('send','Enviar'); ?>
<?php echo form_close(); ?>
    </div>
</div>
