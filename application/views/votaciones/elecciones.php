<div id="content">
<?php echo form_open('votaciones/vota'); ?>

<div id="indicaciones">
    1) Selecciona una planilla por grupo. <br>
    2) Da click en Votar. <br>
</div>

<?php  $i = 1;?>
<?php foreach($eleccion as $elec): ?>
  <div class="toogle"><h1 class="title"> <?php echo $elec['nombre']; ?> </h1></div>
    <div id="<?php echo $i; ?>" class="elecciones">
        <div class="planilla-wrapper" style="max-width:<?php echo count($elec['planilla'])*24 ?>%;">
        <?php foreach ($elec['planilla'] as $plan): ?>
            <div class="flip">
                <div class="card">
                    <div class="planilla face front" style="background-color: <?php echo $plan['color']; ?>; ">
                        <h1> <?php echo $plan['nombre']; ?> </h1>
                        <div class="imagen1" style="background:url('<?php echo base_url() .'webroot/img/'.$plan['logo'] ?>') center no-repeat; background-size: cover;" >
                        </div>
                        <?php $attributes2 = array('name' => 'planilla'.$i, 'id' => 'radbutton', 'value' => $plan['id'] ); ?>
                        <?php echo form_radio($attributes2); ?>
                        <p class="slogan"> <?php echo $plan['slogan']; ?> </p>
                        <span class="flippin">Ver Propuestas</span>
                    </div>
                    <div class="face back">
                        En este espacio van las propuestas de las planillas, las tres mas importantes para que la gente pueda verlas el dia de votaciones
                        <span class="flippin-back">Regresar</span>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
    <br>
<?php $i++;?>
<?php endforeach; ?>
<?php $data = array('id' => 'sub'); ?>
<?php echo form_submit('vota','VOTA', 'id=sub'); ?>
<?php echo form_close(); ?>
</div>
