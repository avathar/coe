


<?php echo anchor(base_url()."index.php/votaciones/graficas",'Participacion por Programa' ); ?>

<script>
$(document).ready(function(){
    $('#form').submit(function(event) {
        event.preventDefault();
        var matricula = $(this).children('input[name=matricula]').val();
        var password = $(this).children('input[name=password]').val();
        var usuario = $('input[name=usuario]').val();
        var form = $(this);
        form.prev().hide();
        if( usuario != '' ){
            var data = {'matricula': matricula, 'password': password, 'usuario' : usuario };
        }else{
            var data = {'matricula': matricula, 'password': password };
        }
        $.ajax({
            type: "POST",
            url: <?php base_url()?>'index.php/votaciones/login',
            data: data,
            success: function(data){
                console.log(data);
                if(data == "true"){
                    window.location = "<?php base_url().'index.php/votaciones/elecciones';?>";
                }else if(data == "voto"){
                    form.prev().html('Ya tienes voto registrado. <br><a href="<?php site_url("votaciones/graficas") ?>"> Ver resultados </a> ').show('easeInOutExpo');
                }else{
                    form.prev().html(' Matricula y/o contraseña inválida').show('easeInOutExpo');
                }
            }
        });
    });
    $('.contingencia').submit(function(event) {
        event.preventDefault();
        var matricula = $(this).children('input[name=matricula]').val();
        var password = $(this).children('input[name=password]').val();
        var usuario = $('input[name=usuario]').val();
        var form = $(this);
        form.prev().hide();
        var data = {'matricula': matricula, 'password': password, 'usuario' : usuario };
        $.ajax({
            type: "POST",
            url: '<?php site_url("votaciones/login") ?>',
            data: data,
            success: function(data){
                if(data == "true"){
                    window.location = "<?php site_url('votaciones/elecciones' );?>";
                }else if(data == "voto"){
                    form.prev().html('Ya tienes voto registrado. <br><a href="<?php site_url("votaciones/graficas") ?>" Ver gráficas </a> ').show('easeInOutExpo');
                }else{
                    form.prev().html(' Matricula y/o contraseña inválida').show('easeInOutExpo');
                }
            }
        });
    });


});
</script>
<div id="login">
    <span style="position: absolute;">
        <?php echo img(array('src'=>'webroot/img/Lock-icon.png','id'=>'lock-img')); ?>
    </span>
    <div class="formcont" id="left">
        <h1> Login </h1>
        <div id='errordialog'> </div>
        <?php
            $this->load->helper('form');
            echo form_open('',array('id'=>'form'));
                echo form_input(array('name'=>'matricula','placeholder'=>'Matricula'));
                echo '<div class="clear"> </div>';
                echo form_password(array('name'=>'password','placeholder'=>'Contraseña'));
                echo '<div class="clear"> </div>';
                echo form_submit('login','Entrar');
            echo form_close( );
        ?>
    </div>
    <div class="formcont" id="right">
        <h1> Login de Contingencia </h1>
        <div id='errordialog'> </div>
        <?php
            $this->load->helper('form');
            echo form_open('',array('id'=>'form', 'class' => 'contingencia'));
                echo form_input(array('name'=>'matricula','placeholder'=>'Matricula Alumno'));
                echo '<div class="clear"> </div>';
                echo form_input(array('name'=>'usuario','placeholder'=>'Usuario COE'));
                echo '<div class="clear"> </div>';
                echo form_password(array('name'=>'password','placeholder'=>'Clave COE'));
                echo '<div class="clear"> </div>';
                echo form_submit('login','Entrar');
            echo form_close( );
        ?>
    </div>

</div>
