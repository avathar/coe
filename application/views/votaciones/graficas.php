<script type="text/javascript" src="http://www.google.com/jsapi">  </script>
<script type="text/javascript">

google.load('visualization', '1');
google.setOnLoadCallback(drawVisualization);

function drawVisualization( ){
    var jsonData = jQuery.ajax({
        url: "<?php echo base_url() ?>index.php/graphs/participacion",
        dataType:"json",
        async: false
    }).responseText;

    var wrapper = new google.visualization.ChartWrapper({
        chartType: 'ColumnChart',
        dataTable: jsonData,
        options: {
            backgroundColor: '#FFFFF0',
            title: 'Participacion por Programa',
            width: 1000,
            height:400,
            chartArea: {width:750},
            isStacked: true,
            hAxis: {
                slantedTextAngle:90,
                maxAlternation: 2,
                textStyle:{
                    fontSize:12
                },
                maxTextLines: 2,
                minTextSpacing: 2
            },
            colors: ['royalblue','FireBrick']
        }
    });
    wrapper.draw('pgraph');

    var jsonData = jQuery.ajax({
        url: "<?php echo base_url() ?>index.php/graphs/totales_profesional",
        dataType:"json",
        async: false
    });
    var wrapper = new google.visualization.ChartWrapper({
        chartType: 'PieChart',
        dataTable: jsonData.responseText,
        options: {
            backgroundColor: '#FFFFF0',
            slices: [{'color':'royalblue'},{'color':'FireBrick'},{'color':'#505050'}],
            title: 'Profesional',
            width: 300,
            height: 300,
            chartArea: {width:"100%",height:"100%",top:50,left:50}
        }
    });
    wrapper.draw('tgraph1');

    var jsonData = jQuery.ajax({
        url: "<?php echo base_url() ?>index.php/graphs/totales_prepa_qro",
        dataType:"json",
        async: false
    });
    var wrapper = new google.visualization.ChartWrapper({
        chartType: 'PieChart',
        dataTable: jsonData.responseText,
        options: {
            backgroundColor: '#FFFFF0',
            slices: [{'color':'royalblue'},{'color':'FireBrick'},{'color':'#505050'}],
            title: 'Prepa Qro',
            width: 300,
            height: 300,
            chartArea: {width:"100%",height:"100%",top:50,left:50}
        }
    });
    wrapper.draw('tgraph2');


    var jsonData = jQuery.ajax({
        url: "<?php echo base_url() ?>index.php/graphs/totales_prepa_cly",
        dataType:"json",
        async: false
    });
    console.log(jsonData.responseText);
    var wrapper = new google.visualization.ChartWrapper({
        chartType: 'PieChart',
        dataTable: jsonData.responseText,
        options: {
            backgroundColor: '#FFFFF0',
            slices: [{'color':'royalblue'},{'color':'FireBrick'},{'color':'#505050'}],
            title: 'Prepa Celaya',
            width: 300,
            height: 300,
            chartArea: {width:"100%",height:"100%",top:50,left:50}
        }
    });
    wrapper.draw('tgraph3');
    console.log(wrapper);


}
</script>

<?php if(isset($voted)): ?>
<div id="indicaciones">
    <p align="center">
        Tu voto ha sido registrado con éxito
        <br>
        Tu participación hace la diferencia
        <br>
        ¡Gracias!
    </p>
</div>
<?php endif; ?>

<div class="links">
    <span class="left"> <?php echo anchor(base_url(),'Regresar a Principal' ); ?> </span>
    <span class="right"> <?php echo anchor("/votaciones/tablas",'Ver Tablas'); ?> </span>
</div>

<div class="graphs">
    <div id="pgraph" align="center"></div>
    <span> <h3> Votos por Sede </h3> </span>
    <div id="sedes">
        <div class="sedeswrapper">
            <div id="tgraph1"></div>
            <div id="tgraph2"></div>
            <div id="tgraph3"></div>
        </div>
    </div>
</div>




