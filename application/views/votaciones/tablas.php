<script type="text/javascript" src="http://www.google.com/jsapi">  </script>
<script type="text/javascript">
google.load('visualization', '1');   // Don't need to specify chart libraries!
google.setOnLoadCallback(drawVisualization);

function drawVisualization( ){
    var jsonData = jQuery.ajax({
        url: "<?php echo base_url() ?>index.php/graphs/tabla_participacion",
        dataType:"json",
        async: false
    }).responseText;

    var wrapper = new google.visualization.ChartWrapper({
        chartType: 'Table',
        dataTable: jsonData,
        options:{
            width: 500
        }
    });
    wrapper.draw('tpgraph');
    var jsonData = jQuery.ajax({
        url: "<?php echo base_url() ?>index.php/graphs/totales_profesional",
        dataType:"json",
        async: false
    });
    var wrapper = new google.visualization.ChartWrapper({
        chartType: 'Table',
        dataTable: jsonData.responseText,
        options:{
            width: 250
        }
    });
    wrapper.draw('tpgraph1');

        var jsonData = jQuery.ajax({
        url: "<?php echo base_url() ?>index.php/graphs/totales_prepa_qro",
        dataType:"json",
        async: false
    });
    var wrapper = new google.visualization.ChartWrapper({
        chartType: 'Table',
        dataTable: jsonData.responseText,
        options:{
            width: 250
        }
    });
    wrapper.draw('tpgraph2');


    var jsonData = jQuery.ajax({
        url: "<?php echo base_url() ?>index.php/graphs/totales_prepa_cly",
        dataType:"json",
        async: false
    });
    var wrapper = new google.visualization.ChartWrapper({
        chartType: 'Table',
        dataTable: jsonData.responseText,
        options:{
            width: 250
        }
    });
    wrapper.draw('tpgraph3');
}
</script>
<div class="links">
    <span class="left"> <?php echo anchor(base_url(),'Regresar a Principal' ); ?> </span>
    <span class="right"> <?php echo anchor("/votaciones/graficas",'Ver Gráficas'); ?> </span>
</div>


<div class="tablas">
    <div id="tpgraph" ></div>
    <div id="sedes">
        <span> <h2> Votos por Sede </h2> </span>
        <span><h3>Profesional</h3></span>
        <div id="tpgraph1"></div>
        <br>
        <span><h3>Prepa Qro</h3></span>
        <div id="tpgraph2"></div>
        <br>
        <span><h3>Prepa Celaya</h3></span>
        <div id="tpgraph3"></div>
    </div>
</div>

